import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Task } from '@app/_models';

@Injectable({
  providedIn: 'root'
})

export class TasksService {

  constructor(private http: HttpClient) { }
  getAll() {
    return this.http.get<Task[]>("https://jsonplaceholder.typicode.com/todos");
    }
    delete(id: number) {
      console.log("mena" +id);
      return this.http.delete(`https://jsonplaceholder.typicode.com/todos/${id}`);
    }
}
