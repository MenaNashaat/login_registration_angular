﻿import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { first } from 'rxjs/operators';

import { User } from '@app/_models';
import { Task} from '@app/_models'
import { UserService, TasksService ,  AuthenticationService } from '@app/_services';

@Component({ templateUrl: 'home.component.html' })
export class HomeComponent implements OnInit, OnDestroy {
    currentUser: User;
    currentUserSubscription: Subscription;
    users: User[] = [];
    tasks: Task[] = [];

    constructor(
        private authenticationService: AuthenticationService,
        private userService: UserService , 
        private tasksService: TasksService
    ) {
        this.currentUserSubscription = this.authenticationService.currentUser.subscribe(user => {
            this.currentUser = user;
        });
    }

    ngOnInit() {
        this.loadAllUsers();
        this.loadAlltasks();
    }

    ngOnDestroy() {
        // unsubscribe to ensure no memory leaks
        this.currentUserSubscription.unsubscribe();
    }

    deleteUser(id: number) {
        this.userService.delete(id).pipe(first()).subscribe(() => {
            this.loadAllUsers()
        });
    }
    deleteTask(id: number) {
        console.log("ok" +id);
        this.tasksService.delete(id).pipe(first()).subscribe(() => {
            this.loadAlltasks()
        });
    }

    private loadAllUsers() {
        this.userService.getAll().pipe(first()).subscribe(users => {
            this.users = users;
        });
    }
    private loadAlltasks() {
        this.tasksService.getAll().pipe(first()).subscribe(tasks => {
            this.tasks = tasks;
        });
    }
}